$('.header__menu').on("click", function () {
    $('.header__list-mob').toggleClass('open')
});
console.log('.site-hero', $('.site-hero'));
// initialise flexslideruu
$('.site-hero').flexslider({
    animation: "fade",
    directionNav: false,
    controlNav: false,
    keyboardNav: true,
    slideToStart: 0,
    animationLoop: true,
    pauseOnHover: false,
    slideShowSpeed: 4000,
});

$('.grid').isotope({
    itemSelector: '.grid-item',
    percentPosition: true,
});

$('.portfolio__filter li').on("click", function(){
    var selection = $(this).text().toLowerCase().trim();
    if (selection === 'all') {
        $('.grid').isotope({ filter: '*' });
    } else {
        $('.grid').isotope({
            filter: '.' + selection,
        });
    }

});
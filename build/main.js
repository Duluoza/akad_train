$(() => {
    const siteHero = $('.site-hero');
    const grid = $('.grid');
    const sideBarTabs = $('.sidebar__tabs');
    const tabContent = $('.tab-content');
    const tab = $('.tab');
    const activeTabClass = 'active-tab';
    
    $('.header__menu').on("click", () => {
        $('.header__list-mob').toggleClass('open')
    });

    if (siteHero.length){
        siteHero.flexslider({
            animation: "fade",
            directionNav: false,
            controlNav: false,
            keyboardNav: true,
            slideToStart: 0,
            animationLoop: true,
            pauseOnHover: false,
            slideShowSpeed: 4000,
        });
    }

    if (grid.length) {
        grid.isotope({
            itemSelector: '.grid-item',
            percentPosition: true,
        });
    }

    $('.portfolio__filter li').on("click", function(){
        const selection = $(this).text().toLowerCase().trim();
        if (selection === 'all') {
            grid.isotope({ filter: '*' });
        } else {
            grid.isotope({
                filter: '.' + selection,
            });
        }
    });

    sideBarTabs.on('click', () => {
        const target = event.target;
        if (target.className.includes('tab')) {
            for (let i=0; i<tab.length; i++) {
                if (target === tab[i]) {
                    showTabsContent(i);
                    break;
                }
            }
        }
    });

    const hideTabsContent = (a) => {
        for (let i = a; i<tabContent.length; i++) {
            $(tabContent[i]).hide();
            $(tab[i]).removeClass(activeTabClass);
        }
    };

    hideTabsContent(1);

    const showTabsContent = (b) => {
        const tabCnt = $(tabContent[b]);
        if (tabCnt.is(':hidden')){
            hideTabsContent(0);
            $(tab[b]).addClass(activeTabClass);
            tabCnt.show();
        }
    };
});
var gulp = require('gulp');
var sass = require('gulp-sass');
var rigger = require('gulp-rigger');
var watch = require('gulp-watch');
var browserSync = require('browser-sync').create();
var tinify = require('gulp-tinify');

gulp.task('tinify', function() {
    gulp.src('src/img/*')
        // .pipe(tinify('TinyPNGAPIKey'))
        .pipe(gulp.dest('build/img'));
});

gulp.task('script:build', function() {
    gulp.src('src/*.js')
        .pipe(gulp.dest('build'));
});

gulp.task('style:build', function () {
    return gulp.src('src/css/*')
        .pipe(sass())
        .pipe(gulp.dest('build/css'));
});

gulp.task('html:build', function () {
    return gulp.src('src/*.html')
        .pipe(rigger())
        .pipe(gulp.dest('build'));
});

gulp.task('reloadBrowser', function (done) {
    browserSync.reload();
    return done();
});

gulp.task('default', function(){

    browserSync.init({
        server: {
            baseDir: "./build"
        }
    });

    gulp.watch('src/**/*', gulp.series('style:build'));
    gulp.watch('src/**/*.html', gulp.series('html:build'));
    gulp.watch('src/img/*', gulp.series('tinify'));
    gulp.watch('src/**/*', gulp.series('reloadBrowser'));
    gulp.watch('src/*.js', gulp.series('script:build'));
});